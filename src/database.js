const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://felicianol3:messengerpassword@messengerdb.wjafp.mongodb.net/glem?retryWrites=true&w=majority";
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const bcrypt = require("bcryptjs")
let db = null;

mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})

const dbIsReady = ()=>{
    return db != null;
};

const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}

const addUser = (username,password,displayName,callback)=>{
    var users = getDb().collection("users");
    users.findOne({username:username}).then(user=>{
        if(user && user.username === username) {
            console.log(`Debug>messengerdb.addUser: Username '${username}' exists!`);
            callback("UserExist");
        }else{
            var hashedpassword = bcrypt.hashSync(password, 10);
            var newUser = {"username": username,"password" : hashedpassword, "displayName" : displayName}
            users.insertOne(newUser,(err,result)=>{
                if(err){
                    console.log("Debug>messengerdb.addUser: error for adding '"+username+"':\n",err);
                    callback("Error");
                }else{
                    console.log("Debug>messengerdb.addUser: a new user added: \n", result.ops[0].username);
                    callback("Success")
                }
            })
        }
    })
}

const addTicket = (name, email, type, priority, description) => {
    var tickets = getDb().collection("tickets");
    var newTicket = {"name": name, "email": email, "type": type, "priority": priority, "description": description};
    tickets.insertOne(newTicket);
}

const  checklogin = async (username,password)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
    if(user!=null && user.username==username){
        console.log("Debug>messengerdb.checklogin-> user found:\n"+JSON.stringify(user))
        return bcrypt.compareSync(password, user.password);
    }
    return false
}


module.exports = {checklogin, addUser, addTicket};