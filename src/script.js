var http = require('http')
var app = require('express')()
var server = http.createServer(app)
const port = process.env.PORT || 8080
server.listen(port);
console.log(`Express HTTP Server is listening at port ${port}`)
app.get('/', (request, response) => {
  console.log("Got an HTTP request")  
  response.sendFile(__dirname+'/index.html')
})
var io = require('socket.io')
var socketio = io.listen(server)
var db = require("./database")
var DataLayer = {
    info: 'Data Layer Implementation',
    async checklogin(username, password) {
        var checklogin_result = await db.checklogin(username,password)
        console.log("Debug>DataLayer.checklogin->result="+checklogin_result)
        return checklogin_result
    },
    addUser(username, password, displayName, callback){
        db.addUser(username,password,displayName, (result) => {
            callback(result)
        })
    },
    addTicket(name, email, type, priority, description) {
        db.addTicket(name, email, type, priority, description);
    }
}
console.log("Socket.IO is listening at port: " + port)
socketio.on("connection", function (socketclient) {
    console.log('A new Socket.io client is connected. ID=' + socketclient.id);

    socketclient.on('login', async (username, password) => {
        var checklogin = await DataLayer.checklogin(username,password)
        if(checklogin) {
            socketclient.authenticated=true;
            socketclient.emit("authenticated");
            socketclient.username = username;
        }
        
    });

    socketclient.on("sendTicket", (name, email, type, priority, description) => {
        DataLayer.addTicket(name, email, type, priority, description);
    });

    socketclient.on("logout", (username) =>{
        socketclient.authenticated=false;
    });

    socketclient.on("register",(username,password, displayName)=> {
        DataLayer.addUser(username,password,displayName,(result)=>{
            socketclient.emit("registration", result)
        })
    });

    socketclient.on('disconnect', function(){
        if(socketclient.username != undefined) {
            socketclient.authenticated=false;
        }
    });
});
